<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'maryam@gmail.com',
            'name' => 'maryam'
        ]);
        User::create([
            'email' => 'sara@yahoo.com',
            'name' => 'sara'
        ]);
        User::create([
            'email' => 'fatima@gmail.com',
            'name' => 'fatima'
        ]);
        User::create([
            'email' => 'armin@yahoo.com',
            'name' => 'armin'
        ]);
    }
}

<?php


namespace App\Services\DomainVerify;


use App\Models\Domain;
use App\Models\User;

interface DomainVerify
{
    public function domainBelongsToUser(User $user, Domain $domain): bool;
}
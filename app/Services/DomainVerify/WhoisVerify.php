<?php


namespace App\Services\DomainVerify;


use App\Models\Domain;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Iodev\Whois\Whois;


class WhoisVerify implements DomainVerify
{

    private $whois;

    public function __construct()
    {

        $this->whois = Whois::create();
    }

    public function domainBelongsToUser(User $user, Domain $domain): bool
    {

        try {
            $info = $this->whois->loadDomainInfo($domain->url);
        } catch (\Exception $domainNotExists) {
            return false;
        }

        $validated = $this->checkDns($info->getNameServers());

        return $validated;
    }

    private function checkDns($nameServers)
    {

        $dnsRegex = Config::get('verification.dns_regex');

        foreach ($nameServers as $nameServer) {
            if (preg_match($dnsRegex, $nameServer)) {
                return true;
            }
        }

        return false;
    }
}
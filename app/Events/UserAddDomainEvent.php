<?php

namespace App\Events;

use App\Models\Domain;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserAddDomainEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $domain;
    public $user;

    /**
     * UserAddDomainEvent constructor.
     * @param Domain $domain
     * @param User $user
     */
    public function __construct(Domain $domain, User $user)
    {
        $this->domain = $domain;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('default');
    }
}

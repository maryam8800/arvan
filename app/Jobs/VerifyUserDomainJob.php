<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Models\User;
use App\Services\DomainVerify\DomainVerify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class VerifyUserDomainJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $domain;
    protected $domainVerifyService;

    /**
     * VerifyUserDomainJob constructor.
     * @param User $user
     * @param Domain $domain
     * @param DomainVerify $domainVerify
     */
    public function __construct(User $user, Domain $domain, DomainVerify $domainVerify)
    {
        $this->user = $user;
        $this->domain = $domain;
        $this->domainVerifyService = $domainVerify;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $verified = $this->domainVerifyService->domainBelongsToUser($this->user, $this->domain);

        $this->user->domains()->updateExistingPivot($this->domain->id, ['verified' => $verified]);

    }
}

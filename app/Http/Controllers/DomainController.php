<?php

namespace App\Http\Controllers;

use App\Events\UserAddDomainEvent;
use App\Models\Domain;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class DomainController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'url' => [
                'required',
                'string',
                'regex:/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['error' => 1, 'data' => $validator->errors()->all()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        //check if domain is verified before
        $domain = Domain::where('url', $request->get('url'))->first();
        if ($domain && $domain->isVerified()) {
            return response()->json(
                ['error' => 1,
                  'data' => 'This domain is already added and verified.'],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        //check if user has domain
        $userHasDomain =  User::whereHas('domains', function ($query) use ($request) {
            $query->where('url', $request->get('url'));
        })->whereId($request->user_id)->get();

        if (!$userHasDomain->isEmpty()) {
            return response()->json(
                ['error' => 1, 'data' => 'You already have this domain.'],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user = User::find($request->get('user_id'));
        $domain = Domain::firstOrCreate(['url' => $request->get('url')]);
        $domain->users()->attach($request->get('user_id'));

        event(new UserAddDomainEvent($domain, $user));

        return response()->json(['error' => 0, 'data' => $domain], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request)
    {

        //validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'domain_id' => 'required|integer|exists:domains,id',
        ]);
        if ($validator->fails()) {
            return response()->json(
                ['error' => 1, 'data' => $validator->errors()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        //check if its verified before
        $domain = Domain::findOrFail($request->get('domain_id'));
        if ($domain->isVerified()) {
            return response()->json(
                ['error' => 1, 'data' => 'This domain has been verified before.'],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user = User::findOrFail($request->get('user_id'));
        event(new UserAddDomainEvent($domain, $user));

        return response()->json(
            ['error' => 0, 'data' => 'Domain verification process started'],
            Response::HTTP_OK
        );
    }
}

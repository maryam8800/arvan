<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class DomainController extends Controller
{
    /**
     * @param $userId
     * @param int $offset
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return view('domains', compact('users'));
    }

    public function show($userId)
    {
        try {
            $user = User::findOrFail($userId);
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'data' => 'User Not found'], Response::HTTP_NOT_FOUND);
        }
        $domains = $user->domains()->get();

        return view('userDomains', compact('domains'));
    }

    public function create($userId)
    {
        return view('createDomain');
    }
}

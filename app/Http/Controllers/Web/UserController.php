<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{


    public function index()
    {

        $users = User::all();
        return view('users', compact('users'));
    }

    public function createView($userId)
    {
        return view('createUser');
    }
}

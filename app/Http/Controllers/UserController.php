<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDomains(Request $request, $userId)
    {

        $offset = $request->input('offset') ?? 0;
        $limit = $request->input('limit') ?? 10;
        if ($limit > 20) {
            $limit = 10;
        }

        try {
            $user = User::findOrFail($userId);
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'data' => 'User Not found'], Response::HTTP_NOT_FOUND);
        }
        $countResult = $user->domains()->count();
        $domains = $user->domains()->offset($offset)->limit($limit)->get();
        return response()->json(
            [
                'error' => 0,
                'data' => $domains->toArray(),'total_records' => $countResult
            ],
            Response::HTTP_OK
        );
    }

}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class DomainCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'url' => 'required|string',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => 'User id is required!',
            'url.required' => 'Name is required!'
        ];
    }
//    public function validate()
//    {
//        $v = Validator::make($this->attributes, $this->rules);
//        if ($v->passes()) return true;
//        $this->errors = $v->messages();
//        return false;
//    }
}

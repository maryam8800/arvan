<?php

namespace App\Listeners;

use App\Events\UserAddDomainEvent;
use App\Jobs\VerifyUserDomainJob;
use App\Services\DomainVerify\WhoisVerify;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class FireUserVerificationJob
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserAddDomainEvent $event)
    {
        VerifyUserDomainJob::dispatch($event->user, $event->domain, (new WhoisVerify()));
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{

    protected $table = 'domains';

    protected $fillable = [
        'url'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function users(){

        return $this->belongsToMany(User::class)->withPivot('verified');
    }

    public function isVerified(){

        if($this->users()->where('verified', true)->first())
            return true;
        else
            return false;
    }

}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class createDomainTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        User::create(['email' => 'aaa@aaa.com', 'name' => 'aaa']);
        $domainData = [
            'user_id' => 1,
            'url' => 'googleeee.com'
        ];

        $response = $this->json('POST', 'api/v1/domains', $domainData);

        $response->assertStatus(200);
    }
}

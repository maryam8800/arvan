@extends('default')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@section('content')

    <div class="row ml-1">
    <span id="success" class="text-success"></span>
    <span id="error" class="text-danger"></span>
    </div>
   <br/><br>

    <div class="">
    <table class="table border"
           id="table"
           data-toggle="table"
           data-height="460">
        <thead>
        <tr>
            <th scope="col">Domain name</th>
            <th scope="col">verified</th>
            <th scope="col">Verify</th>
        </tr>
        </thead>
        <tbody>
        @foreach($domains as $domain)

            <tr>
                <td>{{$domain->url}}</td>
                <td>
                    @if($domain->pivot->verified)
                        <i class="glyphicon glyphicon-ok text-success"></i>
                    @else
                        <i class="glyphicon glyphicon-remove text-danger"></i>
                    @endif
                </td>
                <td><button type="button" value="{{$domain->id}}" class="btn btn-info verify">Verify</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
<br>
    <div>
        <a href="{{ url('/domains/create/' . request()->userId) }}" class="btn btn-info">Create Domain</a>
    </div>

@stop

@section('scripts')
    <link href="https://unpkg.com/bootstrap-table@1.15.5/dist/bootstrap-table.min.css" rel="stylesheet">

    <script src="https://unpkg.com/bootstrap-table@1.15.5/dist/bootstrap-table.min.js"></script>
    <script>
        $(document).ready(function(){

            $(".verify").on("click", function(e){

                $.ajax('/api/v1/domains/verify', {
                    type: 'POST',
                    data: {
                        user_id: '{{request()->userId}}',
                        domain_id: $(this).val()
                    },
                    success: function (data) {
                        $('#error').text('');
                        $('#success').text(data.data);
                    },
                    error: function (error) {
                        $('#success').text('');
                        $('#error').text(error.responseJSON.data);
                    }
                });

            });
        });
    </script>

@endSection()
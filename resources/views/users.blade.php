@extends('default')
@section('content')

    <table class="table border" >
        <thead>
        <tr>
            <th scope="col">User name</th>
            <th scope="col">User email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
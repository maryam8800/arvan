@extends('default')
@section('content')


    <div class="text-center border p-5 col-md-6 offset-3 ">

        <p class="h4 mb-4">Domains</p>

        <p>Choose user that you want to see his domains.</p>

        <select id="selectUser" class="select browser-default custom-select form-control mb-4" >
            <option value="" disabled selected>Choose user</option>

            @foreach ($users as $user)
                <option value={{$user->id}}>{{$user->name}}</option>
            @endforeach

        </select>

        <button class="btn btn-info btn-block" id="submit">Submit</button>

    </div>
@endsection
@section('scripts')


<script>

    $(document).ready(function(){

        $("#submit").on("click", function(e){

             let user_id = $("#selectUser").val();
             if(user_id)
                window.location.replace("domains/" + user_id);
        });
    });
</script>
@endsection
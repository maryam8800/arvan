<header>

    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Project</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample02">
            <ul class="navbar-nav mr-auto">
                @if(request()->getPathInfo() == '/users')
                    <li class="nav-item active" >
                @else
                    <li class="nav-item" >
                 @endif
                    <a href="{{ url('/users')}}" class="nav-link">Users
                    </a>
                </li>
                @if(request()->getPathInfo() == '/domains')
                   <li class="nav-item active" >
                @else
                   <li class="nav-item" >
                @endif
                    <a class="nav-link" href="{{ url('/domains')}}">Domains
                    </a>
                </li>
            </ul>
        </div>
    </nav>

</header>

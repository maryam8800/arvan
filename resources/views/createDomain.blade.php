@extends('default')
@section('content')



    <div class="text-center border p-5 col-md-6 offset-3">

        <p class="h4 mb-4">Add Domain</p>

        <p>Fill in the domain you want to add to your domains.</p>

        <input type="text" id="url" class="form-control mb-4" placeholder="Domain Name">

        <p id="error" class="text-danger"></p>
        <p id="success" class="text-success"></p>

        <button class="btn btn-info btn-block" id="submit">Submit</button>

    </div>
@endsection
@section('scripts')


    <script>

        $(document).ready(function(){

            $("#submit").on("click", function(e){


                $.ajax('/api/v1/domains', {
                    type: 'POST',
                    data: {
                        url: $("#url").val(),
                        user_id: '{{request()->userId}}'
                    },
                    success: function (data) {
                        $('#error').text('');
                        $('#success').text('Domain added successfully');
                    },
                    error: function (error) {
                        $('#success').text('');
                        $('#error').text(error.responseJSON.data);
                    }
                });

            });
        });
    </script>
@endsection